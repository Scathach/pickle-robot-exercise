from phone_wordifier import number_to_words, words_to_number, all_wordification


def test_words_to_number():
    assert words_to_number("") == None
    assert words_to_number(18007246837) == None
    assert words_to_number("1-800PAINTER") == None
    assert words_to_number("1-800PAINTER") == None
    assert words_to_number("1-80-PAINTER") == None
    assert words_to_number("1-800-PASASDAINTER") == None
    assert words_to_number("1-800-P*I>TER") == None

    assert words_to_number("1-800-painter") == "1-800-724-6837"
    assert words_to_number("1-800-PAINTER") == "1-800-724-6837"


def test_number_to_words():
    assert number_to_words(18007246837) == None
    assert number_to_words("") == None
    assert number_to_words("1-800-7246837") == None
    assert number_to_words("01-800-724-6837") == None
    assert number_to_words("1-8002-724-6837") == None
    assert number_to_words("1-802-7242-6837") == None
    assert number_to_words("1-802-724-26837") == None
    assert number_to_words("1-802-724-2ASD") == None


def test_all_wordification():
    assert all_wordification(18007246837) == None
    assert all_wordification("") == None
    assert all_wordification("1-800-7246837") == None
    assert all_wordification("01-800-724-6837") == None
    assert all_wordification("1-8002-724-6837") == None
    assert all_wordification("1-802-7242-6837") == None
    assert all_wordification("1-802-724-26837") == None
    assert all_wordification("1-802-724-2ASD") == None

    assert len(all_wordification("1-800-724-6837")) != 0

    print(all_wordification("1-800-724-6837"))
    if "1-800-PAINTER" in all_wordification("1-800-724-6837"):
        assert True
    else:
        assert False
